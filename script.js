let time_now

function set_title() {
    startTime();
    const title = "English-Dictionary."
    for (var i = 0; i < title.length; i++) {
        document.getElementById("title").innerText = document.getElementById("title").innerText + title[i];
    }
}

function verify() {
    var word = document.getElementById("word_txt").value;
    setDef(word.toLowerCase());
}

function success() {
    if (document.getElementById("word_txt").value === "") {
        document.getElementById('txta').style.boxShadow = "0px 0px 0px 0 rgba(0, 0, 0, 0), 0px 0px 0px 0 rgba(0, 0, 0, 0)"
        document.getElementById('txta').innerText = ""
        document.getElementById('btn_go').disabled = true
        document.getElementById('txta').disabled = true
    } else {
        document.getElementById('btn_go').disabled = false;
        document.getElementById('txta').disabled = false;
    }
}

function setDef(word) {

    document.getElementById('txta').style.boxShadow = "7px 7px 10px 0 rgba(0, 0, 0, 0.4), -7px -7px 10px 0 rgba(8, 97, 230, 0.5)"
    var DATA = data_def;
    var def = "";
    if (word in DATA) {
        for (var i = 0; i < DATA[word].length; i++) {
            def = def + "\n" + (i + 1) + ". " + DATA[word][i];
        }
        word_txt = word.charAt(0).toUpperCase() + word.slice(1);
        document.getElementById('txta').innerText = word_txt + ". \n" + def;
    } else {
        document.getElementById("txta").innerText = "WORD NOT FOUND"
        alert("WORD NOT FOUND")
    }
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    time_now = h;
    //console.log(h + ":" + m + ":" + s);
    if (h >= 8 && h < 20) {
        //Light Theme
        document.body.style.backgroundColor = "#E7E4E0"
        document.getElementById('txta').style.color = "rgb(0, 30, 128)"
        
    } else{
        //Dark Theme
        document.body.style.backgroundColor = "#263238"
        document.getElementById('txta').style.color = "#E7E4E0"
    }
    t = setTimeout(function () {
        startTime()
    }, 1000); s
}


